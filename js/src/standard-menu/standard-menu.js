import StickyMenuBar from "../sticky-menu-bar";
import MobileMenuDrawer from "../mobile-menu-drawer";
import Hamburger from "../hamburger";

const StandardMenu = () => {
    const menuBar = new StickyMenuBar();

    const menuDrawer = new MobileMenuDrawer();

    const hamburger = new Hamburger({
        onOpen: () => {
            menuDrawer.open();
            menuBar.doAttach().lock();
        },
        onClose: () => {
            menuDrawer.close();
            menuBar.unlock();
        },
    });

    return {
        menuBar,
        menuDrawer,
        hamburger,
    };
};

export default StandardMenu;
