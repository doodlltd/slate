/* eslint-disable max-classes-per-file */
class ParallaxImageInstance {
    #timeline = null;

    #options;

    gsap = undefined;

    constructor(element, options, gsap) {
        this.element = element;
        this.#options = options;

        this.gsap = gsap;

        this.#init();
    }

    #init = () => {
        this.image = this.element.querySelector(this.options.imgSelector);

        this.timeline.fromTo(this.image, this.options.from, this.options.to);
    };

    get options() {
        return this.#options;
    }

    // Get or initialise the gsap timeline with scrolltrigger configured
    get timeline() {
        if (this.#timeline) {
            return this.#timeline;
        }

        this.#timeline = this.gsap.timeline({
            scrollTrigger: {
                trigger: this.element,
                start: "top bottom",
                end: "bottom top",
                scrub: 0.5,
                ...this.options.scrollTrigger,
            },
        });

        return this.#timeline;
    }
}

class ParallaxImageBreak {
    elements = [];

    #defaults = {
        selector: ".image-break--parallax-container",
        imgSelector: ":scope .image-break--background",
        scrollTrigger: {},
        from: {
            y: 0,
        },
        to: {
            y: "-50%",
        },
    };

    #options = {};

    gsap = undefined;

    constructor(options = {}) {
        // Merge options with defaults
        this.#options = {
            ...this.#defaults,
            ...options,
        };

        Promise.all([
            import('gsap'),
            import('gsap/ScrollTrigger')
        ]).then(([
            { gsap },
            { default: ScrollTrigger}
        ]) => {
            gsap.registerPlugin(ScrollTrigger);
            this.gsap = gsap;
        }).then(() => {
            this.#init();
        });

    }

    #init = () => {
        /**
         * [].slice.call(**) allows us to iterate over the NodeList from querySelectorAll as if it were an array
         */
        this.elements = [].slice
            .call(document.querySelectorAll(this.options.selector))
            .map((element) => {
                return new ParallaxImageInstance(element, this.options, this.gsap);
            });
    };

    get options() {
        return this.#options;
    }
}

export default ParallaxImageBreak;
