import debounce from "lodash/debounce";

export default class Hamburger {
    #timeline;

    #openState = false;

    options = {
        selector: "button.hamburger",
        onOpen: null,
        onClose: null,
    };

    element = null;

    gsap = undefined;

    constructor(options = {}) {
        this.options = {
            ...this.options,
            ...options,
        };

        import('gsap').then(({ gsap }) => {
            this.gsap = gsap;
        }).then(() => {
            this.#init();
        });
    }

    #init = () => {
        this.element = document.querySelector(this.options.selector);

        if (this.element) {
            this.#createElements()
                .then(this.#positionElements)
                .then(this.#createAnimationTimelines)
                .then(this.#activate);
        } else {
            throw new Error(
                `[Hamburger] No element found using the specified selector (${this.options.selector})`
            );
        }
    };

    #createElements = () => {
        return new Promise((resolve) => {
            this.topBar = document.createElement("span");
            this.middleBar = document.createElement("span");
            this.bottomBar = document.createElement("span");

            this.element.appendChild(this.topBar);
            this.element.appendChild(this.middleBar);
            this.element.appendChild(this.bottomBar);

            resolve();
        });
    };

    #positionElements = () => {
        const buttonHeight = this.element.offsetHeight;
        const barHeight = this.middleBar.offsetHeight;

        this.yShift = buttonHeight / 2 - barHeight / 2;

        this.topBar.style.top = "0px";
        this.middleBar.style.top = `${this.yShift}px`;
        this.bottomBar.style.top = `${2 * this.yShift}px`;
    };

    #createAnimationTimelines = () => {
        // Timing Fns
        const t1 = 0.15;
        const t2 = 0.3;
        // Delay
        const td = 0.1;

        this.timeline
            .to(this.topBar, t1, { y: this.yShift }, 0)
            .to(this.topBar, t2, { rotation: -45 }, t1 + td);

        this.timeline.to(this.middleBar, 0, { opacity: 0 }, t1);

        this.timeline
            .to(this.bottomBar, t1, { y: -this.yShift }, 0)
            .to(this.bottomBar, t2, { rotation: 45 }, t1 + td);
    };

    #setupListeners = () => {
        this.element.addEventListener("click", this.#toggle);
    };

    #activate = () => {
        // Enable public methods
        this.open = this.#open;
        this.close = this.#close;
        this.toggle = this.#toggle;

        this.#setupListeners();
    };

    #open = () => {
        // this.openAnim.play(0);
        this.timeline.play();
        this.#openState = true;

        if (typeof this.options.onOpen === "function") {
            this.options.onOpen();
        }
    };

    #close = () => {
        // this.closeAnim.play(0);
        this.timeline.reverse();
        this.#openState = false;

        if (typeof this.options.onClose === "function") {
            this.options.onClose();
        }
    };

    #_toggle = () => {
        if (this.#openState) {
            this.#close();
        } else {
            this.#open();
        }
    };

    #toggle = debounce(this.#_toggle, 200, { leading: true, trailing: false });

    open = () => {
        // Do nothing until ready
    };

    close = () => {
        // Do nothing until ready
    };

    toggle = () => {
        // Do nothing until ready
    };

    get timeline() {
        // Caching
        if (!this.#timeline) {
            this.#timeline = this.gsap.timeline({ paused: true });
        }

        return this.#timeline;
    }
}
