import React from 'react';
import classNames from 'classnames';
import styles from '../../../../scss/css-modules/typography.module.scss';

const Typography = props => {

    const {
        align = "center",
        children,
        component: Component = 'div',
        wide = false,
        fullWidth = false,
        removeHorizontalPadding = false,
        removeVerticalPadding = false,
        ...rest
    } = props;

    const typographyClasses = classNames(
        styles.typography,
        {
            [styles['align-left']]: align === "left",
            [styles['align-right']]: align === "right",
            [styles.wide]: wide,
            [styles['full-width']]: fullWidth,
            [styles['remove-horizontal-padding']]: removeHorizontalPadding,
            [styles['remove-vertical-padding']]: removeVerticalPadding,
        }
    );

    return (
        <Component className={typographyClasses} {...rest}>
            {children}
        </Component>
    );
};

export default Typography;