import React from 'react';
import classNames from 'classnames';
import styles from '../../../../scss/css-modules/container.module.scss';

const Container = props => {

    const {
        children,
        center = false,
        horizontalPadding = false,
        verticalPadding = false,
        wide = false,
        className
    } = props;

    const containerClass = classNames(
        styles.container,
        {
            [styles.center]: center,
            [styles['horizontal-padding']]: horizontalPadding,
            [styles['vertical-padding']]: verticalPadding,
            [styles.wide]: wide,
        },
        className
    );

    return (
        <div className={containerClass}>
            {children}
        </div>
    );
};

export default Container;