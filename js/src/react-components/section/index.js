import React, { forwardRef } from 'react';
import classNames from 'classnames';
import styles from '../../../../scss/css-modules/section.module.scss';

const Section = forwardRef((props, ref) => {

    const {
        id,
        children,
        component: Component = 'section',
        className,
        removeBottomPadding = false,
        removeTopPadding = false,
        removeLeftPadding = false,
        removeRightPadding = false,
    } = props;

    const classes = classNames(
        styles.section,
        {
            [styles['remove-bottom-padding']]: removeBottomPadding,
            [styles['remove-top-padding']]: removeTopPadding,
            [styles['remove-left-padding']]: removeLeftPadding,
            [styles['remove-right-padding']]: removeRightPadding,
        },
        className,
    );

    return (
        <Component id={id} className={classes} ref={ref}>
            {children}
        </Component>
    );
});

export default Section;