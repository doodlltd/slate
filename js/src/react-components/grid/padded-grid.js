import React, { forwardRef } from 'react';
import classNames from "classnames";
import Grid from ".";
import styles from '../../../../scss/css-modules/grid.module.scss';

const PaddedGrid = (props,ref) => {

    const {
        children,
        component: Component = 'div',
        className,
        ...gridProps
    } = props;

    const {
        slatePaddedGridClassName
    } = styles;

    const paddedGridClasses = classNames(
        styles[slatePaddedGridClassName],
        className,
    );

    return (
        <Component ref={ref} className={paddedGridClasses}>
            <Grid {...gridProps}>
                {children}
            </Grid>
        </Component>
    );
};

export default forwardRef(PaddedGrid);