import React, { forwardRef } from 'react';
import classNames from 'classnames';
import styles from '../../../../scss/css-modules/grid.module.scss';

const Grid = (props,ref) => {

    const {
        children,
        className,
        component: Component = 'div',
        center = false,
        gridClassName, // Usually inherited from PaddedGrid
        sizes = "",
        verticalCenter = false,
    } = props;

    const {
        slateGridClassName
    } = styles;

    // Extract dynamic grid width classname from styles
    const gridSizes = sizes.split(" ").map(size => {
        return styles[`${slateGridClassName}-width-${size}`];
    });

    const gridClasses = classNames(
        styles[slateGridClassName],
        ...gridSizes,
        {
            [styles.center]: center,
            [styles.verticalCenter]: verticalCenter,
        },
        className,
        gridClassName,
    );

    return (
        <Component ref={ref} className={gridClasses}>
            { children }
        </Component>
    );
};

export default forwardRef(Grid);