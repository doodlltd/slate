import React, { forwardRef } from 'react';
import classNames from "classnames";
import styles from '../../../../scss/css-modules/grid.module.scss';

const GridItem = (props,ref) => {

    const {
        children,
        className,
        component: Component = 'div',
        sizes = "",
    } = props;

    const {
        slateGridClassName
    } = styles;

    // Extract dynamic grid item width classname from styles
    const gridSizes = sizes.split(" ").map(size => {
        return styles[`${slateGridClassName}--item-width-${size}`];
    });

    const gridItemClasses = classNames(
        ...gridSizes,
        className
    );

    return (
        <Component ref={ref} className={gridItemClasses}>
            {children}
        </Component>
    );
};

export default forwardRef(GridItem);