/**
 * Utility for sorting elements on the page taking into account their position within
 * the DOM as well as the flex order of each respective element.
 */

function nodeSortFn(a,b) {

    // if both elements have the same parent node we should respect flex order
    if (a.parentNode === b.parentNode) {
        const orderA = +getComputedStyle(a).order;
        const orderB = +getComputedStyle(b).order;
        if (orderA !== orderB) {
            return orderA > orderB;
        }
    }

    // Otherwise we should respect the relative positions in the document.
    // eslint-disable-next-line no-bitwise
    return a.compareDocumentPosition(b) & Node.DOCUMENT_POSITION_PRECEDING;
}


function nodeMergeSort(arr) {
    const m = Math.floor(arr.length / 2);
    if(!m) return arr;
    const arr2 = nodeMergeSort(arr.splice(m));
    const arr1 = nodeMergeSort(arr.splice(0));
    while(arr1.length && arr2.length)
      arr.push((nodeSortFn(arr1[0],arr2[0]) ? arr2 : arr1).shift());
    return arr.concat(arr1, arr2);
}

export default nodeMergeSort;