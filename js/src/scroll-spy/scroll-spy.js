import merge from 'lodash/merge';
import nodeMergeSort from '../utils/node-merge-sort';

export default (options = {}) => {

    const config = merge({
        classes: {
            toggle: 'sspy-visible'
        },
        debug: false,
        selector: '.sspy',
        stagger: 250,
        timeout: 3000,
        batchOptions: {
            start: "top 90%",
            once: true,
            interval: 0.5,
            batchMax: 8
        }
    },options);

    // Output debug messages to console when config.debug is set
    const Debug = (...args) => {
        if (config.debug) {
            // eslint-disable-next-line no-console
            console.log('[sspy]',...args);
        }
    };

    const elements = document.querySelectorAll(config.selector);

    if (!elements.length) {
        Debug('No elements detected for selector',config.selector);
        return;
    }

    Debug(elements.length,"elements detected");

    // Handle any kind of failure. Ensures that no elements that are reliant on scrollspy for visibility will remain
    // hidden as a result.
    const onFailure = () => {
        elements.forEach(el => el.classList.remove('sspy'));
        Debug("Failed on",elements.length,"elements.");
    };

    // If Scrollspy doesn't instantiate before timeout elapses, we should bail out
    const failureTimeout = setTimeout(onFailure,config.timeout);

    Promise.all([
        import('gsap'),
        import('gsap/ScrollTrigger'),
        import('../../../scss/scroll-spy/scroll-spy-lazy.scss')
    ]).then(([
        {
            gsap
        },
        {
            default: ScrollTrigger
        }
    ]) => {
        Debug("ScrollTrigger lib loaded");
        // ScrollTrigger lib has loaded successfully
        clearTimeout(failureTimeout);

        gsap.registerPlugin(ScrollTrigger);

        const Stagger = (_elements, callback, stagger) => {
            // convert decimal to ms
            const milliseconds = stagger > 1 ? stagger : stagger * 1000;
            nodeMergeSort(
                // handle elements like an array
                [].slice.call(
                    _elements
                )
            )
            // .map((pair) => {
            //     return pair[0];
            // })
            .forEach((element,index) => {
                const delay = milliseconds * index;
        
                if (delay) {
                    setTimeout(() => callback(element,index),delay);
                    return;
                }
        
                callback(element,index);
            });
        };

        ScrollTrigger.batch(elements,{
            onEnter: batch => {
                Stagger(batch,(element) => {
                    element.classList.add(config.classes.toggle);
                },config.stagger);
            },
            ...config.batchOptions,
        });

    }).catch(onFailure);

};