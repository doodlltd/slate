/* eslint-disable max-classes-per-file */
class AccordionRow {
    open = false;

    gsap = undefined;

    #options = {
        parentNodeClassName: "accordion--row",
        elementClassName: "accordion--content",
        toggleBtnClass: "accordion--toggle",
        toggleHTML: '<i class="fas fa-caret-down"></i>',
        duration: 0.3,
    };

    #timeline;

    #toggleElement;

    constructor(element, options, gsap) {
        this.element = element;
        this.gsap = gsap;

        if (!this.element) {
            throw new Error("[Accordion Row] Element not found");
        }

        // Merge user options with defaults
        this.#options = {
            ...this.#options,
            ...options,
        };

        this.#setup();
    }

    #setup = () => {
        this.#addElementClasses();
        this.#createToggle();
        this.#setupAnimationTimeline();
    };

    #setupAnimationTimeline = () => {
        this.gsap.set(this.element, { height: 0 });

        // Animate row open
        this.timeline.to(this.element, {
            height: "auto",
            duration: this.#options.duration,
        });

        this.timeline.fromTo(
            this.#toggleElement,
            {
                rotate: 0,
            },
            {
                rotate: -180,
            },
            0
        );
    };

    #addElementClasses = () => {
        this.element.classList.add(this.#options.elementClassName);
        this.element.parentNode.classList.add(
            this.#options.parentNodeClassName
        );
    };

    #createToggle = () => {
        this.#toggleElement = document.createElement("button");
        this.#toggleElement.className = this.#options.toggleBtnClass;
        this.#toggleElement.innerHTML = this.#options.toggleHTML;

        this.#toggleElement.setAttribute("aria-label","Expand Menu");

        this.#toggleElement.addEventListener("click", this.toggleRow);

        this.element.parentNode.appendChild(this.#toggleElement);
    };

    expandRow = () => {
        this.timeline.play().then(() => {
            this.open = true;
        });
    };

    closeRow = () => {
        this.timeline.reverse().then(() => {
            this.open = false;
        });
    };

    toggleRow = () => {
        if (this.open) {
            this.closeRow();
        } else {
            this.expandRow();
        }
    };

    get timeline() {
        // Caching
        if (!this.#timeline) {
            this.#timeline = this.gsap.timeline({ paused: true });
        }

        return this.#timeline;
    }
}

export default class Accordion {
    #options = {
        selector: ".accordion .content",
    };

    rows = [];

    gsap = undefined;

    constructor(options) {
        // Merge user options with defaults
        this.#options = {
            ...this.#options,
            ...options,
        };

        import('gsap').then(({ gsap }) => {
            this.gsap = gsap;
        }).then(() => {
            this.#setupRows();
        });

    }

    #setupRows = () => {
        // borrow the map method from array to use on the nodelist
        this.rows = [].slice
            .call(document.querySelectorAll(this.#options.selector))
            .map((element) => {
                return new AccordionRow(element, this.#options,this.gsap);
            });
    };
}
