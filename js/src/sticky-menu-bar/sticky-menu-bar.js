import debounce from "lodash/debounce";
import isElement from "lodash/isElement";
import domready from "domready";

class StickyMenuBar {
    element = null;

    wrapper = null;

    wrapperHeight = 0;

    isAttached = false;

    locked = false;

    offset = null;

    options = {
        selector: ".sticky-menu-bar",
        wrapperClass: "stick-menu-bar--wrapper",
        attachedClassName: "stuck",
        offset: 65,
        onStuck: null,
        onUnstuck: null,
    };

    constructor(options = {}) {
        this.options = {
            ...this.options,
            ...options,
        };

        this.init();
    }

    init = () => {
        domready(() => {
            this.findAndAttach();
            this.updateWrapperHeight();
            this.setupListeners();
        });
    };

    findAndAttach = () => {
        this.element = document.querySelector(this.options.selector);
        if (!this.element) throw new Error("StickyMenuBar element not found.");
        this.calculateOffset();
        this.createWrapper();
    };

    calculateOffset = () => {
        if (isElement(this.options.offset)) {
            this.offset = this.options.offset.offsetTop;
        } else if (typeof parseFloat(this.options.offset) === "number") {
            this.offset = parseFloat(this.options.offset);
        } else {
            throw new Error("Must pass a DOM Node or number for offset");
        }
    };

    test = () => {
        return new Promise((resolve) => {
            resolve();
        });
    };

    createWrapper = () => {
        this.wrapper = document.createElement("div");
        this.wrapper.classList = this.options.wrapperClass;
        if (this.element.classList.contains("sticky-menu-bar--overlay")) {
            this.wrapper.classList.add("sticky-menu-bar-wrapper--overlay");
        }

        // Wrap menu bar element with 'wrapper'
        this.element.parentNode.insertBefore(this.wrapper, this.element);
        this.wrapper.appendChild(this.element);
    };

    updateWrapperHeight = () => {
        const { height } = this.element.getBoundingClientRect();

        this.wrapperHeight = `${height}px`;

        this.wrapper.style.height = this.wrapperHeight;
    };

    setupListeners = () => {
        window.addEventListener("scroll", this.debouncedHandleScroll);
    };

    // Determine whether the menu bar should be attached
    shouldAttach = () => {
        return window.scrollY > this.offset;
    };

    doAttach = () => {
        // Best time to recalculate wrapper height is immediately before attachment because we know that's the original state of the element
        this.updateWrapperHeight();

        this.element.classList.add(this.options.attachedClassName);
        if (typeof this.options.onAttach === "function") {
            this.options.onAttach(this);
        }
        this.isAttached = true;

        return this;
    };

    doDetach = () => {
        this.element.classList.remove(this.options.attachedClassName);
        if (typeof this.options.onDetach === "function") {
            this.options.onDetach(this);
        }
        this.isAttached = false;

        return this;
    };

    handleAttachment = () => {
        if (this.locked) return;
        if (!this.isAttached && this.shouldAttach()) {
            this.doAttach();
        } else if (this.isAttached && !this.shouldAttach()) {
            this.doDetach();
        }
    };

    handleScroll = () => {
        this.handleAttachment();
    };

    debouncedHandleScroll = debounce(this.handleScroll, 200);

    lock = () => {
        this.locked = true;
        return this;
    };

    unlock = () => {
        this.locked = false;
        this.handleAttachment();
        return this;
    };
}

export default StickyMenuBar;
