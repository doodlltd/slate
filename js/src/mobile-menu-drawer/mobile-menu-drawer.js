export default class MobileMenuDrawer {
    options = {
        selector: ".mobile-menu-drawer",
        duration: 0.4,
    };

    element = null;

    #timeline;

    gsap = undefined;

    constructor(options) {
        this.options = {
            ...this.options,
            ...options,
        };

        import('gsap').then(({ gsap }) => {
            this.gsap = gsap;
        }).then(() => {
            this.#init();
        });
    }

    #init = () => {
        this.element = document.querySelector(this.options.selector);

        if (!this.element) {
            throw new Error(
                `[MMD] No element found using the specified selector (${this.options.selector})`
            );
        }
        this.#createAnimationTimelines();
    };

    #createAnimationTimelines = () => {
        const { duration } = this.options;

        this.timeline.fromTo(
            this.element,
            { x: "0%" },
            { x: "-100%", duration }
        );
    };

    open = () => {
        this.timeline.play();
    };

    close = () => {
        this.timeline.reverse();
    };

    get timeline() {
        // Caching
        if (!this.#timeline) {
            this.#timeline = this.gsap.timeline({ paused: true });
        }

        return this.#timeline;
    }
}
