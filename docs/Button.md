# Slate Button Styles Documentation

## Introduction

Slate SCSS toolkit delivers a robust, versatile, and fully customizable way to style interactive elements such as buttons. This applies to various HTML tags, including `<button>`, `<a>`, `<input type="button">`, and `<input type="submit">`.

Here are examples of each:

```html
<button type="button" class="button" aria-pressed="false" aria-label="Interactive Button">Button</button>
<a href="#" class="button" role="button" aria-label="Link acting as a Button">Button</a>
<input type="button" class="button" aria-label="Input Button" value="Button">
<input type="submit" class="button" aria-label="Input Submit" value="Submit">
```

When using a `<button>` element, always specify the `type` attribute. `button` is typical, though `submit` is needed when submitting form data.

For accessibility, ARIA attributes are highly recommended. Include the `role="button"` attribute for `<a>` elements styled as buttons.

## Button Variations

The Slate Button Styles come with several variations. These are dictated by different CSS classes that can be appended to your HTML elements.

Here are examples of the different variations:

### Default Button
```html
<button type="button" class="button" aria-pressed="false" aria-label="Default Button">Button</button>
```

### Contained Button
```html
<button type="button" class="button contained" aria-pressed="false" aria-label="Contained Button">Contained Button</button>
```

### Good and Bad Buttons
```html
<button type="button" class="button good" aria-pressed="false" aria-label="Good Button">Good Button</button>
<button type="button" class="button bad" aria-pressed="false" aria-label="Bad Button">Bad Button</button>
```

## Disabled Button

A disabled button can be created by adding the "disabled" attribute to the button. This grays out the button, indicating that users cannot interact with it.

```html
<button type="button" class="button" disabled aria-label="Disabled Button">Disabled Button</button>
<button type="button" class="button primary contained" disabled aria-label="Disabled Contained Button">Disabled Contained Button</button>
```

Each class defines different attributes such as display, background, border style, font size, letter spacing, color, etc. These styles can be fully customized through SCSS variables.

## Button States

### Hover State

Hover states are auto-generated using button mixins. The background color transitions on mouse hover, ensuring sufficient contrast between the background color and the text color. This hover state color can be manually adjusted using SCSS variables, if needed.

### Active State

The active state indicates when a button is being clicked or pressed. Like hover states, active state colors are auto-generated and can be customized if required.

**Note:** Use the `aria-pressed` attribute for interactive buttons. The attribute represents the current state of the button, where "false" means the button is not currently pressed, and "true" means the button is currently pressed.

### Loading State

The loading state adds a loading spinner to the button. This is achieved by adding the `action_loading` class to the button and appending a spinner element (like a `<span>` with the `action_loading--spinner` class) via JavaScript. This state should use the `aria-busy="true"` attribute to indicate that the button is currently "busy".

For example:
```html
<button type="button" class="button action_loading" aria-busy="true" aria-label="Loading Button">Loading<span class="action_loading--spinner"></span></button>
```

The spinner's appearance can be customized using the loader-spinner mixin.

The button styles, variations, and states have been designed to offer a high degree of flexibility and customization, allowing for different design requirements. For further customization, adjust the SCSS variables according to your project's unique needs, and remember to always use appropriate ARIA attributes for accessibility.