module.exports = {
    ...require("@doodl/common-js-config/prettier-config"),
    trailingCommaPHP: false,
};
