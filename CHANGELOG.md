# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

### [1.23.5](https://bitbucket.org/doodlltd/slate/compare/v1.23.4...v1.23.5) (2024-07-24)


### Bug Fixes

* fix float rules for images added via SilverStripe's CMS editor ([895be6e](https://bitbucket.org/doodlltd/slate/commit/895be6ec0c58b17b6ca3c79cb4622725814a1fcc))


### Improvements

* add an option to explicitly force an image to float on small screen sizes ([1a0acb2](https://bitbucket.org/doodlltd/slate/commit/1a0acb2c94f76c9b48d6eb6fa84e54633ad2143c))

### [1.23.4](https://bitbucket.org/doodlltd/slate/compare/v1.23.3...v1.23.4) (2024-07-23)


### Bug Fixes

* add missing max-width default to styled buttons ([2ac92e7](https://bitbucket.org/doodlltd/slate/commit/2ac92e7c802e9962ad90a880a443829eb2335188))

### [1.23.3](https://bitbucket.org/doodlltd/slate/compare/v1.23.2...v1.23.3) (2024-07-23)


### Improvements

* improve the default overflow behaviour of styled buttons ([7b5462b](https://bitbucket.org/doodlltd/slate/commit/7b5462b7b709e9eef242df4d95136947e3573269))

### [1.23.2](https://bitbucket.org/doodlltd/slate/compare/v1.23.1...v1.23.2) (2024-07-19)

### [1.23.1](https://bitbucket.org/doodlltd/slate/compare/v1.23.0...v1.23.1) (2024-01-12)


### Bug Fixes

* resolve string interpolation error causing MediaQueryPlugin error ([87ddf11](https://bitbucket.org/doodlltd/slate/commit/87ddf11faf60bb7aa949f5413ad8123d944454de))

## [1.23.0](https://bitbucket.org/doodlltd/slate/compare/v1.22.16...v1.23.0) (2023-09-17)


### Features

* Add explicit configuration for button active state ([d92312d](https://bitbucket.org/doodlltd/slate/commit/d92312d9ba4bbe10b2784b2808e7cc09e16adb2d))

### [1.22.16](https://bitbucket.org/doodlltd/slate/compare/v1.22.15...v1.22.16) (2023-07-03)


### Bug Fixes

* Strip units from $slate-hamburger-height definition sanity check ([d22ed18](https://bitbucket.org/doodlltd/slate/commit/d22ed18668bba72fbd66ccdd441a89c419a1389a))

### [1.22.15](https://bitbucket.org/doodlltd/slate/compare/v1.22.14...v1.22.15) (2023-06-26)


### Bug Fixes

* Resolve 'respond' mixin causing 'invalid CSS' error – workaround known bug in libsass ([5f1f04b](https://bitbucket.org/doodlltd/slate/commit/5f1f04b53d2c0f0fbbff3d0016c4db8a68dd63db))
* Resolve non-percentage arguments for SCSS scale-color fn throwing errors in dart sass ([4fcd689](https://bitbucket.org/doodlltd/slate/commit/4fcd689df6cf79ada215b9d851f76a3fa993a42d))

### [1.22.14](https://bitbucket.org/doodlltd/slate/compare/v1.22.13...v1.22.14) (2023-06-14)


### Bug Fixes

* Ensure that contained-button mixin generates a :visited state ([4057d0d](https://bitbucket.org/doodlltd/slate/commit/4057d0d48304c6e1d964d346c459611c8bbf0e3b))

### [1.22.13](https://bitbucket.org/doodlltd/slate/compare/v1.22.12...v1.22.13) (2023-06-01)

### [1.22.12](https://bitbucket.org/doodlltd/slate/compare/v1.22.11...v1.22.12) (2023-06-01)


### Bug Fixes

* Inconsistency in default sticky menu bar styling compared to standard menu bar ([f47952b](https://bitbucket.org/doodlltd/slate/commit/f47952b9cede966c8439103e16dc03673bf2606b))

### [1.22.11](https://bitbucket.org/doodlltd/slate/compare/v1.22.10...v1.22.11) (2023-06-01)


### Bug Fixes

* Handling of highlight colour mapping when highlight colour is not one of primary colour or secondary colour ([d6de398](https://bitbucket.org/doodlltd/slate/commit/d6de3987af317ec6023f4f8445efa020db15bd15))

### [1.22.10](https://bitbucket.org/doodlltd/slate/compare/v1.22.9...v1.22.10) (2023-06-01)


### Bug Fixes

* Layout Section selector ([e386659](https://bitbucket.org/doodlltd/slate/commit/e38665926243185a358e77620d4ae769a989cf2b))

### [1.22.9](https://bitbucket.org/doodlltd/slate/compare/v1.22.8...v1.22.9) (2023-05-31)

### [1.22.8](https://bitbucket.org/doodlltd/slate/compare/v1.22.7...v1.22.8) (2022-11-17)

### [1.22.7](https://bitbucket.org/doodlltd/slate/compare/v1.22.6...v1.22.7) (2022-10-06)


### Bug Fixes

* Add missing vertical padding prop to Container react component ([9baf852](https://bitbucket.org/doodlltd/slate/commit/9baf85234a3f920469869c09227057b88ecfc0b1))

### [1.22.6](https://bitbucket.org/doodlltd/slate/compare/v1.22.5...v1.22.6) (2022-06-08)


### Bug Fixes

* Fix critical typo in button background colour generator mixin ([b436504](https://bitbucket.org/doodlltd/slate/commit/b436504b82094d7ff8cc5870f9d70379de868a76))

### [1.22.5](https://bitbucket.org/doodlltd/slate/compare/v1.22.4...v1.22.5) (2022-05-26)


### Bug Fixes

* Resolve colour behaviour of default contained buttons ([9fe80a6](https://bitbucket.org/doodlltd/slate/commit/9fe80a6411fb0af3b4e5cc6fa5a52a7a14531fc2))

### [1.22.4](https://bitbucket.org/doodlltd/slate/compare/v1.22.3...v1.22.4) (2022-04-07)

### [1.22.3](https://bitbucket.org/doodlltd/slate/compare/v1.22.2...v1.22.3) (2022-02-13)


### Bug Fixes

* Flag default breakpoints as SCSS defaults ([f380ddc](https://bitbucket.org/doodlltd/slate/commit/f380ddce65ef6287b040e2a2a796ebf0eecbc29d))

### [1.22.2](https://bitbucket.org/doodlltd/slate/compare/v1.22.1...v1.22.2) (2021-12-22)


### Bug Fixes

* Assign property inheritance to variable properties if the value is a property map ([71a9287](https://bitbucket.org/doodlltd/slate/commit/71a9287b2846bbccb4ffd19e185d26542dbdca3c))
* Ensure $key is not required to assign a variable property ([02c419f](https://bitbucket.org/doodlltd/slate/commit/02c419f0ce5c58fc329b13c1520c23813179d7d4))
* Nested map-get doesn't work on older Sass compilers ([1b9c98b](https://bitbucket.org/doodlltd/slate/commit/1b9c98be844a6bc096aa7d9a1f9c4560d0ebd3ec))

### [1.22.1](https://bitbucket.org/doodlltd/slate/compare/v1.22.0...v1.22.1) (2021-12-22)

## [1.22.0](https://bitbucket.org/doodlltd/slate/compare/v1.21.5...v1.22.0) (2021-12-16)


### Features

* Add basic style customisation through custom CSS properties ([d549499](https://bitbucket.org/doodlltd/slate/commit/d5494998c7afc634a217dca874ab02b17f31eafc))
* Add react components with modular styles ([0b5a5eb](https://bitbucket.org/doodlltd/slate/commit/0b5a5eb3f90079bce7c30005881162261dff05eb))

### [1.21.5](https://bitbucket.org/doodlltd/slate/compare/v1.21.4...v1.21.5) (2021-09-01)


### Bug Fixes

* Resolve missing accessible name from accordion toggle button ([71d22fa](https://bitbucket.org/doodlltd/slate/commit/71d22fa87461efd1d506e11b3c3610ea7fa57b20))

### [1.21.4](https://bitbucket.org/doodlltd/slate/compare/v1.21.3...v1.21.4) (2021-09-01)

### [1.21.3](https://bitbucket.org/doodlltd/slate/compare/v1.21.2...v1.21.3) (2021-08-19)

### [1.21.2](https://bitbucket.org/doodlltd/slate/compare/v1.21.1...v1.21.2) (2021-07-08)

### [1.21.1](https://bitbucket.org/doodlltd/slate/compare/v1.21.0...v1.21.1) (2021-07-07)


### Bug Fixes

* ScrollSpy failure is non-blocking ([3936f04](https://bitbucket.org/doodlltd/slate/commit/3936f04af499abe91b31462ee09fae8fd3f6752e))

## [1.21.0](https://bitbucket.org/doodlltd/slate/compare/v1.20.3...v1.21.0) (2021-07-06)


### Features

* Add ScrollSpy ([4d3c3d8](https://bitbucket.org/doodlltd/slate/commit/4d3c3d8281134d450eab3121a3fa636f965c9fd2))

### [1.20.3](https://bitbucket.org/doodlltd/slate/compare/v1.20.2...v1.20.3) (2021-07-02)

### [1.20.2](https://bitbucket.org/doodlltd/slate/compare/v1.20.1...v1.20.2) (2021-07-01)


### Bug Fixes

* SilverStripe textarea and select fields not inheriting container field widths ([1288686](https://bitbucket.org/doodlltd/slate/commit/128868676fca04a1d7aebde6e145a8a7fb3210fa))

### [1.20.1](https://bitbucket.org/doodlltd/slate/compare/v1.20.0...v1.20.1) (2021-06-10)


### Bug Fixes

* Change selector inheritance through [@extend](https://bitbucket.org/extend) to make use of %placeholder selectors, preventing pollution of scope ([feb61f3](https://bitbucket.org/doodlltd/slate/commit/feb61f33bd38f4d08895ad746fad3e0639f53fc3))
* Resolve adjacent button selector bug ([8f64aa2](https://bitbucket.org/doodlltd/slate/commit/8f64aa2c792ffff2a3531109627bb1a9762f7900))

## [1.20.0](https://bitbucket.org/doodlltd/slate/compare/v1.19.3...v1.20.0) (2021-06-10)


### Features

* Improve responsive behaviour of buttons ([d19d33c](https://bitbucket.org/doodlltd/slate/commit/d19d33cec5c1ea5eba7279621d43bb794816ce86))


### Bug Fixes

* Fix default menu dropdown shadow transparency being too dark ([9c51113](https://bitbucket.org/doodlltd/slate/commit/9c51113960849b2defc053a220df9b40ca4ca442))

### [1.19.3](https://bitbucket.org/doodlltd/slate/compare/v1.19.2...v1.19.3) (2021-05-18)


### Bug Fixes

* Hide overflow from image break background image ([f4b5617](https://bitbucket.org/doodlltd/slate/commit/f4b56177c5fca5217e4ccc4a95bb9624b429140d))

### [1.19.2](https://bitbucket.org/doodlltd/slate/compare/v1.19.1...v1.19.2) (2021-05-18)


### Bug Fixes

* Hide overflow from image break background image ([db3e75c](https://bitbucket.org/doodlltd/slate/commit/db3e75ce9e782c6fe2afc399a7bde2389d4deec3))

### [1.19.1](https://bitbucket.org/doodlltd/slate/compare/v1.19.0...v1.19.1) (2021-05-18)


### Bug Fixes

* Asterisk on required fields displays as slate-bad-colour by default ([cd2c4d0](https://bitbucket.org/doodlltd/slate/commit/cd2c4d0414ba26eb604902eac803cb42ef60b788))

## [1.19.0](https://bitbucket.org/doodlltd/slate/compare/v1.18.0...v1.19.0) (2021-05-13)


### Features

* add stylings for label astericks (pull request [#10](https://bitbucket.org/doodlltd/slate/issues/10)) ([7c0a8f1](https://bitbucket.org/doodlltd/slate/commit/7c0a8f1851fb01541c9386af9ed1876dc2c0b78f))

## [1.18.0](https://bitbucket.org/doodlltd/slate/compare/v1.17.2...v1.18.0) (2021-05-10)


### Features

* Add menu bar styling improvements including child menu defaults ([bf5aa80](https://bitbucket.org/doodlltd/slate/commit/bf5aa80929fc86e1b3caefaa95eb10d7e5b287b0))

### [1.17.2](https://bitbucket.org/doodlltd/slate/compare/v1.17.1...v1.17.2) (2021-05-10)


### Bug Fixes

* Add missing text-columns utility class ([4f938aa](https://bitbucket.org/doodlltd/slate/commit/4f938aa81ca4d233d0db577538deb687fd6093ce))

### [1.17.1](https://bitbucket.org/doodlltd/slate/compare/v1.17.0...v1.17.1) (2021-05-10)


### Bug Fixes

* Add missing heading placeholders to for-all-headings mixin ([1eb60dd](https://bitbucket.org/doodlltd/slate/commit/1eb60ddf32348cd350bc86f39e2d1f55ba1ac6e3))

## [1.17.0](https://bitbucket.org/doodlltd/slate/compare/v1.16.0...v1.17.0) (2021-04-23)


### Features

* add caption class to silverstripe editor styles ([1918f53](https://bitbucket.org/doodlltd/slate/commit/1918f5345e3fa91ff2711413d1174c2d238c03db))


### Bug Fixes

* edit silverstripe editor styles to include .center ([0a47918](https://bitbucket.org/doodlltd/slate/commit/0a479181c08dabc16b3c68ef6e31bd7173185cdf))

## [1.16.0](https://bitbucket.org/doodlltd/slate/compare/v1.15.0...v1.16.0) (2021-04-20)


### Features

* Add gutter modifiers to layout ([38d0128](https://bitbucket.org/doodlltd/slate/commit/38d012839fcc4fdf0cba1ee20ad259686802dc09))


### Bug Fixes

* Remove erroneous !important flag from grid trailing margin ([baad5d7](https://bitbucket.org/doodlltd/slate/commit/baad5d7c08fceaf679ca5c33272012632c4d66a9))

## [1.15.0](https://bitbucket.org/doodlltd/slate/compare/v1.14.1...v1.15.0) (2021-04-19)


### Features

* add text-transform property and variable to menu-bar ([cef4495](https://bitbucket.org/doodlltd/slate/commit/cef4495c726367da543162702443d29be8f3bd1f))
* add wide class to container ([b64dfc3](https://bitbucket.org/doodlltd/slate/commit/b64dfc31b6b2774f90e8c423918048d4d2e509f4))


### Bug Fixes

* add domready to stickymenubar to ensure getBoundingClientRect has access to correct height ([9f637e8](https://bitbucket.org/doodlltd/slate/commit/9f637e84b7dc122864f9e09f0fb605bb82f36db1))
* add missing '&' to .wide class on typography ([add32d6](https://bitbucket.org/doodlltd/slate/commit/add32d609999db994fed4ac16caea007cf213139))

### [1.14.1](https://bitbucket.org/doodlltd/slate/compare/v1.14.0...v1.14.1) (2021-04-13)


### Bug Fixes

* Implement missing text-muted utility class ([1b8296a](https://bitbucket.org/doodlltd/slate/commit/1b8296a245d1ccb34f44989da5bf803a7bd23458))
* Resolve typos ([93bbba3](https://bitbucket.org/doodlltd/slate/commit/93bbba36c5b537fd8c4704fcf109af6e6fe06adb))

## [1.14.0](https://bitbucket.org/doodlltd/slate/compare/v1.13.0...v1.14.0) (2021-04-12)


### Features

* Add responsive font sizing ([612af96](https://bitbucket.org/doodlltd/slate/commit/612af96533f630d4033bf6ff0147ab4b7d87821c))


### Bug Fixes

* Classname .section applies section layout behaviour to other elements ([5fba303](https://bitbucket.org/doodlltd/slate/commit/5fba303fa0a9d8887bc89f342fa693c55d86f489))

## [1.13.0](https://bitbucket.org/doodlltd/slate/compare/v1.12.0...v1.13.0) (2021-04-08)


### Features

* add responsive padding to sections ([074dee9](https://bitbucket.org/doodlltd/slate/commit/074dee90acacae1974cfa63718d32e683736954a))
* add section medium padding variables to layout variable folder ([1ae1994](https://bitbucket.org/doodlltd/slate/commit/1ae1994ef3f4c93b78a7fd7b72b341f38442eb71))


### Bug Fixes

* typo in layout.scss variables ([1046365](https://bitbucket.org/doodlltd/slate/commit/1046365275da9997e71af79c2b71d3a6511f9633))

## [1.12.0](https://bitbucket.org/doodlltd/slate/compare/v1.11.2...v1.12.0) (2021-04-01)


### Features

* add padding remove rules to section class ([80376ff](https://bitbucket.org/doodlltd/slate/commit/80376ff2076627af1cffd0fd85c91aabcc6880e4))
* utils file with helper rules ([bab67b1](https://bitbucket.org/doodlltd/slate/commit/bab67b13370ae122989815c5730c09188c7fa5e8))


### Bug Fixes

* remove bottom padding rule from utils ([803aaac](https://bitbucket.org/doodlltd/slate/commit/803aaac56a205314ebd683d91986009ae24578b2))

### [1.11.2](https://bitbucket.org/doodlltd/slate/compare/v1.11.1...v1.11.2) (2021-03-23)

### [1.11.1](https://bitbucket.org/doodlltd/slate/compare/v1.11.0...v1.11.1) (2021-03-23)

## [1.11.0](https://bitbucket.org/doodlltd/slate/compare/v1.10.0...v1.11.0) (2021-03-23)


### Features

* Add non-parallax 'Image-break'. 'Parallax-image-break' becomes an augmentation via the addition of a simple parallax container ([d27bfb9](https://bitbucket.org/doodlltd/slate/commit/d27bfb9975327f4ef44aa7bb70a09e4cecdd4a3b))

## [1.10.0](https://bitbucket.org/doodlltd/slate/compare/v1.9.1...v1.10.0) (2021-03-11)


### Features

* add stylings for a spinner animation when action buttons are in the loading state ([a4a97d1](https://bitbucket.org/doodlltd/slate/commit/a4a97d1e4d8bc6c279193d97e3ea6fff9dcba5d3))

### [1.9.1](https://bitbucket.org/doodlltd/slate/compare/v1.9.0...v1.9.1) (2021-03-10)


### Bug Fixes

* Remove typo on form loading state fieldset ([4180a40](https://bitbucket.org/doodlltd/slate/commit/4180a40a981bbf1ae122169c1ea807234be0a5a1))

## [1.9.0](https://bitbucket.org/doodlltd/slate/compare/v1.8.2...v1.9.0) (2021-03-08)


### Features

* add functions folder and colour function to library ([3b08767](https://bitbucket.org/doodlltd/slate/commit/3b087676aadf40e2af4420a5e3ad5937c15e7ee0))
* export variables for listbox-field (+1 squashed commit) ([147816e](https://bitbucket.org/doodlltd/slate/commit/147816e8a17e3c7901147e6a8ea314ad5b63fa93))

### [1.8.2](https://bitbucket.org/doodlltd/slate/compare/v1.8.1...v1.8.2) (2021-01-11)


### Bug Fixes

* Prevent colour change from 'stuck' menu items trickling down to children ([e8e786b](https://bitbucket.org/doodlltd/slate/commit/e8e786b73f2a9eac18af33afb565724d06938fba))

### [1.8.1](https://bitbucket.org/doodlltd/slate/compare/v1.8.0...v1.8.1) (2020-12-17)


### Bug Fixes

* Change accordion default selectors ([27345f4](https://bitbucket.org/doodlltd/slate/commit/27345f43b16b4e65b2e0e38e5245717777bca3b4))
* Mitigate broken padding/margin on accordion toggle svg (icon) ([87a2470](https://bitbucket.org/doodlltd/slate/commit/87a24701be760c3979046bb51ebdd8b241bd1fee))

## [1.8.0](https://bitbucket.org/doodlltd/slate/compare/v1.7.0...v1.8.0) (2020-12-09)


### Features

* Add linting config ([1f3db5a](https://bitbucket.org/doodlltd/slate/commit/1f3db5af9f39df2fe04f31d7ff30bcc868698898))


### Bug Fixes

* Remove console logging from Accordion ([623af51](https://bitbucket.org/doodlltd/slate/commit/623af5156bd325a0acc07256bfa2cb1c66587c10))
* Resolve circular dependency issue ([077fc03](https://bitbucket.org/doodlltd/slate/commit/077fc03418204277089f046fc2d2d4a6464d9a56))

## [1.7.0](https://bitbucket.org/doodlltd/slate/compare/v1.6.6...v1.7.0) (2020-12-09)


### Features

* Add Accordion component ([6cf41aa](https://bitbucket.org/doodlltd/slate/commit/6cf41aae42c0ac5b2602cd845c2aaa75e59ab9ae))


### Bug Fixes

* Improvements to mobile menu drawer defaults ([5814692](https://bitbucket.org/doodlltd/slate/commit/5814692122e2deea8f155bea58cc6c1a5e0e8cde))

### [1.6.6](https://bitbucket.org/doodlltd/slate/compare/v1.6.5...v1.6.6) (2020-12-09)


### Bug Fixes

* Add missing holder error class to fields ([ce0898f](https://bitbucket.org/doodlltd/slate/commit/ce0898fd43e33fe2778244b97e43e08dc56ec935))

### [1.6.5](https://bitbucket.org/doodlltd/slate/compare/v1.6.4...v1.6.5) (2020-11-20)


### Bug Fixes

* Add missing variables import ([824cd4a](https://bitbucket.org/doodlltd/slate/commit/824cd4a95db2c95b371fea956753febbbe10ba57))
* Missing spacing import ([a347223](https://bitbucket.org/doodlltd/slate/commit/a3472232d80b55e61ae38ebcea9c240ef3bd76a5))
* remove max-width from optionset ([ec273dc](https://bitbucket.org/doodlltd/slate/commit/ec273dc07c8c244a647a5769dbe330fef05a6059))

### [1.6.4](https://bitbucket.org/doodlltd/slate/compare/v1.6.3...v1.6.4) (2020-11-19)


### Bug Fixes

* Add hover colour override to contained button borders via config ([8aea331](https://bitbucket.org/doodlltd/slate/commit/8aea33189ccbc7ff3790518632f4cb25bf8ddd5f))
* Remove opinionated striped background on optionset form fields ([9ea67e1](https://bitbucket.org/doodlltd/slate/commit/9ea67e12da7672d1fcc60b7127f81669d2f41df4))

### [1.6.3](https://bitbucket.org/doodlltd/slate/compare/v1.6.2...v1.6.3) (2020-11-19)


### Bug Fixes

* Add missing description list styling ([32800a1](https://bitbucket.org/doodlltd/slate/commit/32800a1bddd68a0fe4ffd157fff18baeaec05070))

### [1.6.2](https://bitbucket.org/doodlltd/slate/compare/v1.6.1...v1.6.2) (2020-11-18)


### Bug Fixes

* Add extra list defaults and hooks ([099870f](https://bitbucket.org/doodlltd/slate/commit/099870fabaecd9b7fd4d6a04f5e74aeebc5eaff4))
* Add missing config for silverstripe messages ([06195c4](https://bitbucket.org/doodlltd/slate/commit/06195c41061ab02856881817fe315d2cedf38b81))

### [1.6.1](https://bitbucket.org/doodlltd/slate/compare/v1.6.0...v1.6.1) (2020-11-18)


### Bug Fixes

* Update base anchor font weight to inherit to resolve inconsistencies ([2f2dc32](https://bitbucket.org/doodlltd/slate/commit/2f2dc3251bb172adeb8b81e475094e0ac7140580))

## [1.6.0](https://bitbucket.org/doodlltd/slate/compare/v1.5.0...v1.6.0) (2020-11-18)


### Features

* Add extra hooks to table styling ([bf7fe21](https://bitbucket.org/doodlltd/slate/commit/bf7fe2160acfca95faa14444a0c023379ef14181))

## [1.5.0](https://bitbucket.org/doodlltd/slate/compare/v1.4.2...v1.5.0) (2020-11-18)


### Features

* Add placeholder selectors to base components ([e39dfc1](https://bitbucket.org/doodlltd/slate/commit/e39dfc174b19ea3f50b02af8b49eec4f1ca89663))

### [1.4.2](https://bitbucket.org/doodlltd/slate/compare/v1.4.1...v1.4.2) (2020-11-18)


### Bug Fixes

* missing variable assignment ([f08fa2f](https://bitbucket.org/doodlltd/slate/commit/f08fa2f861d14e739e281e91f826230828d3f17c))

### [1.4.1](https://bitbucket.org/doodlltd/slate/compare/v1.4.0...v1.4.1) (2020-11-18)


### Bug Fixes

* Add blockquote styling ([72446f5](https://bitbucket.org/doodlltd/slate/commit/72446f53896b20b68b59d68cef28678b9b156607))

## [1.4.0](https://bitbucket.org/doodlltd/slate/compare/v1.3.4...v1.4.0) (2020-11-18)


### Features

* Add alignment helper classes to grid ([1839405](https://bitbucket.org/doodlltd/slate/commit/1839405270645d596d4e162b81d7997ca62d5c88))

### [1.3.4](https://bitbucket.org/doodlltd/slate/compare/v1.3.3...v1.3.4) (2020-11-17)


### Bug Fixes

* Resolve disabled button styling causing compile to fail ([872e856](https://bitbucket.org/doodlltd/slate/commit/872e856de98974a5c2bd798ef938aabc3e643ee1))

### [1.3.3](https://bitbucket.org/doodlltd/slate/compare/v1.3.2...v1.3.3) (2020-11-17)


### Bug Fixes

* Add trailing margin to forms that are rendered with React by the @doodl/ss-react-forms library ([06b536e](https://bitbucket.org/doodlltd/slate/commit/06b536efb6d4d6a03ebc92562e4e13a50d38281c))

### [1.3.2](https://bitbucket.org/doodlltd/slate/compare/v1.3.1...v1.3.2) (2020-11-16)


### Bug Fixes

* Add loading & disabled state styling to Silverstripe form components ([3d5250b](https://bitbucket.org/doodlltd/slate/commit/3d5250be2bfa5ec58a8cc76d7c4be7c22aad8392))

### [1.3.1](https://bitbucket.org/doodlltd/slate/compare/v1.3.0...v1.3.1) (2020-11-06)


### Bug Fixes

* Resolve issue with glitchy menu when opens less than full-width ([6068c4a](https://bitbucket.org/doodlltd/slate/commit/6068c4a81fa9f1e055b2b4c9c471c9b39c631c5e))

## [1.3.0](https://bitbucket.org/doodlltd/slate/compare/v1.2.2...v1.3.0) (2020-11-02)


### Features

* Add handling of picture elements to parallax-image-break ([56625d2](https://bitbucket.org/doodlltd/slate/commit/56625d2f3479a691fa9216112b1109b3a406313d))
* Integrate SilverStripe editor styles ([8ed2d3b](https://bitbucket.org/doodlltd/slate/commit/8ed2d3bdc7db3166cc67082baafe63c7de604c66))

### [1.2.2](https://bitbucket.org/doodlltd/slate/compare/v1.2.1...v1.2.2) (2020-10-21)


### Bug Fixes

* broken style caused by missing brace ([f0320f9](https://bitbucket.org/doodlltd/slate/commit/f0320f9699cfa9103133fc9192413b99248e63de))

### [1.2.1](https://bitbucket.org/doodlltd/slate/compare/v1.2.0...v1.2.1) (2020-10-21)


### Bug Fixes

* resolve issue caused by trailing margin on ParallaxImageBreak ([1914079](https://bitbucket.org/doodlltd/slate/commit/191407939cef7ff44b65d55822fc48e5f5de3b9b))

## [1.2.0](https://bitbucket.org/doodlltd/slate/compare/v1.1.3...v1.2.0) (2020-10-21)


### Features

* Add 'contrast' module ([5c8e376](https://bitbucket.org/doodlltd/slate/commit/5c8e376396a42c7ef5c2755e1c96737f36629f9c))
* Add ParallaxImageBreak ([c4bf489](https://bitbucket.org/doodlltd/slate/commit/c4bf4895576332e388e59464a1d9c8a7b5257cd0))

### [1.1.3](https://bitbucket.org/doodlltd/slate/compare/v1.1.2...v1.1.3) (2020-08-28)


### Bug Fixes

* Sticky Menu attachment bug caused by resize on mobile browsers ([e77fa2e](https://bitbucket.org/doodlltd/slate/commit/e77fa2edd1c5a0d96a1723f5fa6b720b4a4401f2))
* Sticky Menu Bar wrapper z-index blocking mobile menu items ([bf25399](https://bitbucket.org/doodlltd/slate/commit/bf253997ccfe4309224e055d849de9d88a1263a4))

### [1.1.2](https://bitbucket.org/doodlltd/slate/compare/v1.1.1...v1.1.2) (2020-08-26)


### Bug Fixes

* Add missing trailing margin for tables ([84c21a9](https://bitbucket.org/doodlltd/slate/commit/84c21a9aac566d24a8782f7cf748b4e97eab83fc))

### [1.1.1](https://bitbucket.org/doodlltd/slate/compare/v1.1.0...v1.1.1) (2020-08-13)


### Bug Fixes

* Optimize import of GSAP for better tree-shaking ([a12c199](https://bitbucket.org/doodlltd/slate/commit/a12c199fe0f1193798c2af6efe665b25d1385480))
* Resolve glitchy hamburger animation by reverting to a single animation timeline ([ef93968](https://bitbucket.org/doodlltd/slate/commit/ef9396850d31d8a48cc2499df06a96e2f9ca0d57))

## [1.1.0](https://bitbucket.org/doodlltd/slate/compare/v1.0.4...v1.1.0) (2020-08-06)


### Features

* Add standard menu configuration ([516d07d](https://bitbucket.org/doodlltd/slate/commit/516d07dd29c3a97e04ef70317ffca05868e1cc8a))

### [1.0.4](https://bitbucket.org/doodlltd/slate/compare/v1.0.3...v1.0.4) (2020-08-05)


### Bug Fixes

* Add missing import to form variable declarations ([6c158c1](https://bitbucket.org/doodlltd/slate/commit/6c158c1fc099bd344b58d4ef35834fafef79d284))

### [1.0.3](https://bitbucket.org/doodlltd/slate/compare/v1.0.1...v1.0.3) (2020-08-05)

### 1.0.1 (2020-08-05)
