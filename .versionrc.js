module.exports = {
    releaseCommitMessageFormat: "chore(release): {{currentTag}} [skip ci]",
    types: [
        {
            type: "feat",
            section: "Features",
        },
        {
            type: "impr", 
            section: "Improvements",
        },
        {
            type: "fix",
            section: "Bug Fixes",
        },
        {
            type: "refactor",
            section: "Refactoring",
        },
        {
            type: "test",
            section: "Tests",
            hidden: true,
        },
        {
            type: "build",
            section: "Build System",
            hidden: true,
        },
        {
            type: "ci",
            hidden: true,
        },
    ],
};
